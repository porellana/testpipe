FROM python:3

#WORKDIR /usr/src/app

RUN apk update
 
RUN apk add --no-cache curl jq python3 py3-pip gcc g++ make firefox-esr 
 
WORKDIR /app
 
RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz
RUN tar -x geckodriver -zf geckodriver-v0.24.0-linux64.tar.gz -O > /usr/bin/geckodriver
RUN chmod +x /usr/bin/geckodriver
RUN rm geckodriver-v0.24.0-linux64.tar.gz

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
#RUN python3 -m pip install robotframework

COPY . .
 
 
CMD [ "robot", "Autoidentify.robot" ]